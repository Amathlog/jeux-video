﻿using UnityEngine;
using System.Collections;


public class PlatformerCharacter2D : MonoBehaviour 
{
	bool facingRight = true;							// For determining which way the player is currently facing.

	[SerializeField] float maxSpeed = 10f;				// The fastest the player can travel in the x axis.
	[SerializeField] float jumpForce = 8f;				// Amount of force added when the player jumps.	
	[SerializeField] float jetpackForce = 16f;		 	// Amount of force added when the player uses the jetpack.

	[Range(0, 1)]
	[SerializeField] float crouchSpeed = .36f;			// Amount of maxSpeed applied to crouching movement. 1 = 100%
	
	[SerializeField] float airControl = 1f;				// Whether or not a player can steer while jumping;
	[SerializeField] LayerMask whatIsGround;			// A mask determining what is ground to the character
	[SerializeField] LayerMask whatIsWall;				// A mask determining what is a wall to the character
	[SerializeField] float jumpTime = 0.25f;			// Maximum time in which you can press the jump button
	[SerializeField] int maxJumps = 2;					// Maximum number of jumps without touching the ground
	[SerializeField] bool showMaxJumpHeight;			// When true, a line is drawn to show the maximum height of the simple jump

	Transform groundCheck;								// A position marking where to check if the player is grounded.
	float groundedRadius = .2f;							// Radius of the overlap circle to determine if grounded
	bool grounded = false;								// Whether or not the player is grounded.
	Transform ceilingCheck;								// A position marking where to check for ceilings
	float ceilingRadius = .01f;							// Radius of the overlap circle to determine if the player can stand up
	Transform wallCheck;								// A position marking where to check if the player is touching a wall.
	float walledRadius = .2f;							// Radius of the overlap circle to determine if the player is touching a wall
	bool walled = false;								// Whether or not the player is touching a wall.
	Animator anim;										// Reference to the player's animator component.
	Platformer2DUserControl controller;					// Reference to the player's controller component.
	uint jumpCounter = 0;								// Number of jumps done without touching the ground

    void Awake()
	{
		// Setting up references.
		groundCheck = transform.Find("GroundCheck");
		ceilingCheck = transform.Find("CeilingCheck");
		wallCheck = transform.Find("WallCheck");
		anim = GetComponent<Animator>();
		controller = GetComponent<Platformer2DUserControl> ();
	}

	void OnValidate()
	{
		if (maxJumps < 0) maxJumps = 0;
		if (jumpTime < 0f) jumpTime = 0f;
	}

	void FixedUpdate()
	{
		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
		anim.SetBool("Ground", grounded);

		walled = Physics2D.OverlapCircle(wallCheck.position, walledRadius, whatIsWall);

		// Set the vertical animation
		anim.SetFloat("vSpeed", rigidbody2D.velocity.y);

		if(showMaxJumpHeight)
			Debug.DrawLine (new Vector3 (transform.position.x - 100, transform.position.y + jumpForce * jumpTime, 0f), new Vector3 (transform.position.x + 100, transform.position.y + jumpForce * jumpTime, 0f), Color.red);
	}


	public void Move(float move, bool crouch, bool jump)
	{


		// If crouching, check to see if the character can stand up
		if(!crouch && anim.GetBool("Crouch"))
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if( Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround))
				crouch = true;
		}

		// Set whether or not the character is crouching in the animator
		anim.SetBool("Crouch", crouch);

		//only control the player if grounded or airControl is turned on
		if(grounded || airControl > 0f)
		{
			// Reduce the speed if crouching by the crouchSpeed multiplier
			move = (crouch ? move * crouchSpeed : move);
			move = (grounded ? move : move * airControl);

			// The Speed animator parameter is set to the absolute value of the horizontal input.
			anim.SetFloat("Speed", Mathf.Abs(move));

			// Move the character
			rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);
			
			// If the input is moving the player right and the player is facing left...
			if(move > 0 && !facingRight)
				// ... flip the player.
				Flip();
			// Otherwise if the input is moving the player left and the player is facing right...
			else if(move < 0 && facingRight)
				// ... flip the player.
				Flip();
		}

		jumpCounter = (grounded ? 0 : (uint)Mathf.Max(1, jumpCounter));

		if (walled && jump) {
			anim.SetBool ("Ground", false);
			StartCoroutine (JumpRoutine ());
			StartCoroutine (WallJumpRoutine());
		} else {
			// If the player should jump...
			if (jump) {
				if (jumpCounter < maxJumps) {
					// Add a vertical force to the player.
					anim.SetBool("Ground", false);
					//rigidbody2D.AddForce(new Vector2(0f, jumpForce));
					StartCoroutine(JumpRoutine());
					
					++jumpCounter;
				} else {
					// Add a vertical force to the player.
					anim.SetBool("Ground", false);
					StartCoroutine(JetpackRoutine());
				}
			}
		}

	}


	IEnumerator JetpackRoutine()
	{
		rigidbody2D.velocity = Vector2.zero;

		while(controller.JumpButtonDown)
		{
			//Add a constant force every frame of the jump
			rigidbody2D.AddForce(new Vector2(0f, jetpackForce));
			yield return null;
		}
		
	}

	IEnumerator WallJumpRoutine() {
		float timer = 0f;
				
		float latJmpForce = jumpForce * 60;

		if (facingRight)
			latJmpForce = -latJmpForce;

		while(timer < jumpTime)
		{
			rigidbody2D.AddForce(new Vector2(latJmpForce, 0f));
			timer += Time.deltaTime;
			latJmpForce /= 1.05f;
			yield return null;
		}
				
	}

	IEnumerator JumpRoutine()
	{

		//Set the gravity to zero and apply the force once
		float startGravity = rigidbody2D.gravityScale;
		rigidbody2D.gravityScale = 0;
		rigidbody2D.velocity = new Vector2(0f, jumpForce);
		float timer = 0f;
		
		while(controller.JumpButtonDown && timer < jumpTime)
		{
			timer += Time.deltaTime;
			yield return null;
		}
		
		//Set gravity back to normal at the end of the jump
		rigidbody2D.gravityScale = startGravity;

	}
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	//Return true if grounded
	public bool isGrounded(){
		return grounded;
	}

	//Return true if on jetpack(ie. no more multiple jumps remaining)
	public bool onJetpack(){
		return (jumpCounter == maxJumps);
	}
}
