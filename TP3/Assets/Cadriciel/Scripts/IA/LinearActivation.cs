﻿using System;
using AForge.Neuro;

[Serializable]
public class LinearFunction : IActivationFunction, ICloneable{

	public double Function(double x){
		if (x > 1.0)
			return 1;
		else if (x < 0.0)
			return 0;
		else
			return x;
	}

	public double Derivative(double x){
		if (x < 0 || x > 1)
			return 0;
		else
			return 1;
	}

	public double Derivative2(double y){
		if (y < 0 || y > 1)
			return 0;
		else
			return 1;
	}

	public object Clone(){
		return new LinearFunction ();
	}

	public override string ToString ()
	{
		return string.Format ("[LinearFunction]");
	}

}
