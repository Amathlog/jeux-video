﻿using System.Linq;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class QLearning {

	private double[,] Q_values = null;		//Values of the matrix
	private double alpha = 0.3;				//Learning rate
	private double gamma = 0.9;				//Discount factor
	private int nbStates = 1;				//Number of states (number of lines in matrix)
	private int nbActions = 1;				//Number of actions (number of columns in matrix)
	private int lastState = 1;				//Index of the last state
	private double epsilon = 0.1;			//Parameter for randomization action (not used at the end)

	//Quick access to Q values
	public double Q(int i, int j){
		return Q_values [i, j];
	}

	//Serialize the Q_values into a file
	public void Save(string fileName) {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(fileName);

		bf.Serialize(file, Q_values);
		file.Close();
	}

	//Load a serialized file to our Q_values
	//Return if no file (then Q_values = null)
	private void Load(string fileName) {
		if (!File.Exists(fileName)) return;
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(fileName, FileMode.Open);

		Q_values = (double[,])bf.Deserialize(file);
		file.Close();
	}

	//Called at the beginning of the experience, or the race.
	//If the file doesn't exist (no learning so far), we randomize the init.
	public void init(string fileName, int nbStates, int nbActions, int lastState){
		this.nbStates = nbStates;
		this.nbActions = nbActions;
		this.lastState = lastState;
		Load (fileName);
		if (Q_values == null) {
			randInit ();
		}
	}

	//The initial random initialization
	private void randInit(){
		Q_values = new double[nbStates,nbActions];
		System.Random rand = new System.Random ();
		for (int i = 0; i < nbStates; i++) {
			for (int j = 0; j < nbActions; j++) {
				//The last state is initialize to 0
				Q_values [i,j] = (i == lastState) ? 0 : rand.NextDouble ();
			}
		}
	}

	//The update function for Q. See the lesson if more explanation needed
	//Briefly, when the agent reach a state, it updates the previous state
	//Here nextState is the actual state when the function is called and state is the previous
	//state, and action the action that leaded to nextState.
	public void updateQ(int state, int action, double reward, int nextState, bool sarsaMethod){
		if(!sarsaMethod)
			Q_values [state,action] += alpha * (reward + gamma * maxValueQ (nextState) - Q_values [state,action]);
		else
			Q_values [state,action] += alpha * (reward + gamma * Q_values[nextState, chooseNextAction(nextState)] - Q_values [state,action]);
	}

	//Return the max of the function Q with a given state.
	private double maxValueQ(int state){
		double max = double.MinValue;
		for (int i = 0; i < nbActions; i++) {
			if (Q_values [state,i] > max) {
				max = Q_values [state,i];
			}
		}
		return max;
	}

	//Return the best action (given Q and a state)
	private int maxValueQState(int state){
		double max = double.MinValue;
		int action = 0;
		for (int i = 0; i < nbActions; i++) {
			if (Q_values [state,i] > max) {
				max = Q_values [state,i];
				action = i;
			}
		}
		return action;
	}

	//Give the max value for an action at a given state
	//During learning, have a espilon chance to select a random action
	public int chooseNextAction(int currentState, bool learning = false){
		if (learning) {
			System.Random rand = new System.Random ();
			if (rand.NextDouble () < epsilon) {
				return rand.Next (0, nbActions);
			}
		}
		return maxValueQState (currentState);
	}

}
