﻿using System;
using AForge.Neuro;

[Serializable]
public class AffineFunction : IActivationFunction, ICloneable{

	private double a;
	private double b;

	public AffineFunction(double a, double b){
		this.a = a;
		this.b = b;
	}

	public double Function(double x){
		return a * x + b;
	}

	public double Derivative(double x){
		return a;
	}

	public double Derivative2(double y){
		return a;
	}

	public object Clone(){
		return new AffineFunction (a, b);
	}

	public override string ToString ()
	{
		return string.Format ("[AffineFunction]");
	}
}
