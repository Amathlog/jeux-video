﻿

public class Matrix{
	private float[,] values;
	public int rows{ get; private set;}
	public int cols{ get; private set;}

	public float this[int i, int j]{
		get{ return values [i, j]; }
		set{values[i,j] = value;}
	}

	public Matrix(int rows, int cols){
		this.rows = rows;
		this.cols = cols;
		values = new float[rows, cols];
	}

	public static Matrix operator +(Matrix m1, Matrix m2){
		if (!sameSize (m1, m2))
			throw(new System.Exception("Operator + failed, matrixes are not the same size"));
		Matrix res = new Matrix (m1.rows, m1.cols);
		for (int i = 0; i < m1.rows; i++) {
			for (int j = 0; j < m1.cols; j++) {
				res [i, j] = m1 [i, j] + m2 [i, j];
			}
		}
		return res;
	}

	public static Matrix operator *(float alpha, Matrix m1){
		Matrix res = new Matrix (m1.rows, m1.cols);
		for (int i = 0; i < m1.rows; i++) {
			for (int j = 0; j < m1.cols; j++) {
				res [i, j] = m1 [i, j] * alpha;
			}
		}
		return res;
	}

	public static Matrix operator -(Matrix m1, Matrix m2){
		return m1 + ((-1) * m2);
	}

	public static Matrix operator *(Matrix m1, Matrix m2){
		if (!productOk (m1, m2))
			throw(new System.Exception("Operator * failed, matrixes are not compatible"));
		Matrix res = new Matrix (m1.rows, m2.cols);
		for (int i = 0; i < m1.rows; i++) {
			for (int j = 0; j < m2.cols; j++) {
				float aux = 0;
				for (int k = 0; k < m1.cols; k++) {
					aux += m1 [i, k] * m2 [k, j];
				}
				res [i, j] = aux;
			}
		}
		return res;
	}

	public static Matrix inPlaceMult(Matrix m1, Matrix m2){
		if (!sameSize (m1, m2))
			throw(new System.Exception("Operator .* failed, matrixes are not the same size"));
		Matrix res = new Matrix (m1.rows, m2.cols);
		for (int i = 0; i < m1.rows; i++) {
			for (int j = 0; j < m2.cols; j++) {
				res [i, j] = m1 [i, j] * m2 [i, j];
			}
		}
		return res;
	}

	public Matrix transpose(){
		Matrix res = new Matrix (cols, rows);
		for (int i = 0; i < cols; i++) {
			for (int j = 0; j < rows; j++) {
				res [i, j] = values [j, i];
			}
		}
		return res;
	}

	private static bool sameSize(Matrix m1, Matrix m2){
		return m1.rows == m2.rows && m1.cols == m2.cols;
	}

	private static bool productOk(Matrix m1, Matrix m2){
		return m1.cols == m2.rows;
	}

	public string toString(){
		string res = "";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				res += values [i, j] + " ";
			}
			res += '\n';
		}
		return res;
	}

	public void randInit(){
		System.Random rand = new System.Random ();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				values [i,j] = (float)rand.NextDouble ();
			}
		}
	}

	public static Matrix filled(float value, int rows, int cols){
		Matrix res = new Matrix (rows, cols);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				res [i, j] = value;
			}
		}
		return res;
	}

	public void idInit(){
		if(rows != cols)
			throw(new System.Exception("IdImpossible"));
		for (int i = 0; i < rows; i++) {
				values [i, i] = 1;
		}
	}

	public Matrix addOnes(bool onRow){
		Matrix res;
		if (onRow) {
			res = new Matrix (this.rows + 1, this.cols);
			for (int i = 0; i < res.rows; i++) {
				for (int j = 0; j < res.cols; j++) {
					if (i == 0)
						res [i, j] = 1;
					else
						res [i, j] = this [i - 1, j];
				}
			}
		} else {
			res = new Matrix (this.rows, this.cols + 1);
			for (int i = 0; i < res.rows; i++) {
				for (int j = 0; j < res.cols; j++) {
					if (j == 0)
						res [i, j] = 1;
					else
						res [i, j] = this [i, j-1];
				}
			}
		}
		return res;
	}

	public Matrix removeFirstRow(){
		Matrix res = new Matrix (this.rows - 1, this.cols);
		for (int i = 0; i < res.rows; i++) {
			for (int j = 0; j < res.cols; j++) {
				res [i, j] = this [i + 1, j];
			}
		}
		return res;
	}

	public Matrix removeFirstCols(){
		Matrix res = new Matrix (this.rows, this.cols - 1);
		for (int i = 0; i < res.rows; i++) {
			for (int j = 0; j < res.cols; j++) {
				res [i, j] = this [i, j + 1];
			}
		}
		return res;
	}
}

