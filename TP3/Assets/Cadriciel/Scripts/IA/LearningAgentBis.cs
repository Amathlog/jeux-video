﻿using UnityEngine;
using System.Collections;
using AForge.Neuro;
using AForge.Math.Random;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class LearningAgentBis : MonoBehaviour
{

	public const int nbActions = 15;
	public string fileName;
	public GameObject statesBoxes;
	public bool learning = true;

	//Other varaibles
	public float maxDistance = 40.0f;
	public float timeBetweenActions = 1.0f;
	public float classicReward = 0.0f;
	public float boxReward = 1.0f;
	public float outsideReward = -1.0f;
	public float threshHoldNotMoving = 1.5f;
	public float max_x;
	public float max_z;

	//Reward
	private float reward;

	//Features
	private double[] currentState;
	private int nbFeatures = 6;

	//Neural Network useful attributes
	private ActivationNetwork network;
	private AForge.Neuro.Learning.BackPropagationLearning backprop;

	private QLearning qLearning;

	private TextureDetector textureDetector;
	private CarController car;

	private float time_saved;

	private int currentAction;
	private int currentBox;
	private Vector3 currentPosition;

	// Use this for initialization
	void Awake () {
		currentState = new double[nbFeatures];
		if (!enabled)
			return;
		Application.runInBackground = true;
		textureDetector = GetComponent<TextureDetector> ();
		if(File.Exists(fileName)){
			network = (ActivationNetwork)ActivationNetwork.Load (fileName);
		} else {
			network = new ActivationNetwork (new TanhFunction(), nbFeatures, 30, nbActions);
			((ActivationLayer)network.Layers [network.Layers.Length - 1]).SetActivationFunction (new AffineFunction(1,0));
			//Random initialization of the weights
			Neuron.RandRange = new AForge.Range(-0.1f, 0.1f);
			network.Randomize ();
			foreach (Layer l in network.Layers) {
				foreach (ActivationNeuron n in l.Neurons) {
					n.Threshold = 0;
				}
			}
			network.Save (fileName);
		}
		backprop = new AForge.Neuro.Learning.BackPropagationLearning (network);
		backprop.LearningRate = 0.1;
		backprop.Momentum = 0.5;
		int childCount = 0;
		foreach (Transform child in statesBoxes.transform) {
			child.GetComponent<StateScriptBis> ().StateNumber = childCount;
			childCount++;
		}
		car = GetComponent<CarController>();
		currentBox = 0;
		getValues (car.transform.position, car.transform.forward);
		time_saved = Time.time;
		chooseNextAction (true);
		reward = classicReward;
		currentPosition = car.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Time.time - time_saved > timeBetweenActions) {
			if (Vector3.Distance(car.transform.position, currentPosition) < threshHoldNotMoving) {
				//Debug.Log ("Detected not moving");
				reward = outsideReward;
			}
			//Compute new value of Q
			//Debug.Log("Before update : " + toStringOutput());
			updateNetwork();
			chooseNextAction (true);
			//Debug.Log("After update : " + toStringOutput());
			time_saved = Time.time;
			reward = classicReward;
			showStateAction ();
			currentPosition = car.transform.position;
		}
	}

	void FixedUpdate(){
		//		Debug.Log ("Action = " + currentAction);
		float h = 0;
		float v = 0;
		if (currentAction % 3 == 1)
			v = 0.5f;
		else if (currentAction % 3 == 2)
			v = 1;
		if (currentAction / 3 == 1)
			h = 0.5f;
		else if (currentAction / 3 == 2)
			h = -0.5f;
		else if (currentAction / 3 == 3)
			h = 1;
		else if (currentAction / 3 == 4)
			h = -1;
		//		Debug.Log ("h = " + h);
		//		Debug.Log ("v = " + v);
		car.Move(h,v,0,false,false);
		if (car.CurrentSpeed < 0) {
			time_saved -= timeBetweenActions;
			reward -= outsideReward;
		}
	}

	void OnDestroy(){
		if (!enabled)
			return;
		network.Save (fileName);
	}

	public void boxEncoutered(bool alreadyTriggered, float angle){
		if (!enabled)
			return;
		if (alreadyTriggered)
			reward = outsideReward;
		else
			reward = (boxReward * (1.0f - angle/30.0f));

		currentBox++;
		//Debug.Log ("New box = " + currentBox);
	}

	private bool isOutsideTheRoad(){
		if (textureDetector.surfaceIndex != 1) {
			reward += outsideReward;
			return true;
		}
		return false;
	}

	public void Outside(){
		if (!enabled)
			return;
		reward = outsideReward;
		updateNetwork ();
		Application.LoadLevel (Application.loadedLevel);
	}


	private void getValues(Vector3 position, Vector3 forward){
		Transform currentBoxTrans = statesBoxes.transform.GetChild (currentBox);
		Vector3 currentBoxPosition = currentBoxTrans.position;
		Vector3 currentBoxForward = currentBoxTrans.forward;
		Vector3 nextBoxForward = statesBoxes.transform.GetChild((currentBox+1)%statesBoxes.transform.childCount).forward;
		currentState[0] = System.Convert.ToDouble(Vector3.Angle (forward, currentBoxForward)/180.0f);
		if (Vector3.Dot (Vector3.Cross (forward, currentBoxForward), transform.up) <= 0.0f)
			currentState[0] *= -1;
		currentState[1] = System.Convert.ToDouble(Vector3.Angle (currentBoxForward,nextBoxForward)/180.0f);
		if (Vector3.Dot (Vector3.Cross (currentBoxForward, nextBoxForward), transform.up) <= 0.0f)
			currentState[1] *= -1;
		currentState[2] = System.Convert.ToDouble(Vector3.Distance (currentBoxPosition, position)/maxDistance - 0.5)*2;
		currentState[3] = System.Convert.ToDouble(car.CurrentSpeed/car.MaxSpeed)*2;
		currentState [4] = System.Convert.ToDouble (car.transform.position.x / max_x);
		currentState [5] = System.Convert.ToDouble (car.transform.position.z / max_z);
		//THE LOOOOOOOGS
		/*Debug.Log("Angle entre Voiture et Boite = " + Mathf.Sign(Vector3.Dot (Vector3.Cross (forward, currentBoxForward), transform.up)) * Vector3.Angle (forward, currentBoxForward));
		Debug.Log("Angle entre Boite et Boite = " + Mathf.Sign(Vector3.Dot (Vector3.Cross (currentBoxForward, nextBoxForward), transform.up)) * Vector3.Angle (currentBoxForward,nextBoxForward));
		Debug.Log("Vitesse = " + speed);
		Debug.Log ("Distance = " + Vector3.Distance (currentBoxPosition, position));*/
	}

	//Get the next action and return its Q-value
	private void chooseNextAction(bool learning){
		double[] aux = network.Compute(currentState);
		if (learning) {
			System.Random rand = new System.Random ();
			if (rand.NextDouble () < 0.1) {
				currentAction = rand.Next (0, nbActions);
				return;
			}
		}
		double max = double.MinValue;
		for (int i = 0; i < nbActions; i++) {
			if (aux [i] > max) {
				max = aux [i];
				currentAction = i;
			}
		}
	}

	private void updateNetwork(){
		int save_act = currentAction;
		double[] save_state = currentState;
		double[] save_output = network.Output;
		getValues(car.transform.position, car.transform.forward);
		save_output [save_act] = reward + 0.9 * network.Output [currentAction];
		/*string res = "{";
		for (int i = 0; i < nbFeatures - 1; i++) {
			res += string.Format("{0:N2}",save_state[i]) + ",";
		}
		Debug.Log(res + string.Format("{0:N2}",save_state[nbFeatures - 1]) + "}");
		res = "{";
		for (int i = 0; i < nbActions - 1; i++) {
			res += string.Format("{0:N2}",save_output[i]) + "|";
		}
		Debug.Log(res + string.Format("{0:N2}",save_output[nbActions - 1]) + "}");*/
		Debug.Log ("Valeur changée : " + save_output [save_act]);
		backprop.Run(save_state, save_output);
	}

	private void showStateAction(){
		string res = "";
		if (currentAction % 3 == 1)
			res += "HALF ACCELERATE ";
		else if (currentAction % 3 == 2)
			res += "ACCELERATE ";
		else
			res += "NO ACCELERATION ";
		if (currentAction / 3 == 1)
			res += "HALF RIGHT";
		else if (currentAction / 3 == 2)
			res += "HALF LEFT";
		else if (currentAction / 3 == 3)
			res += "RIGHT";
		else if (currentAction / 3 == 4)
			res += "LEFT";
		else
			res += "FORWARD";
		string box = currentBox.ToString ();
		Debug.Log ("Current state : " + toStringState() + " Box : " + box + " Action : " + res + "("+currentAction+")" + '\n' + "Q = " + toStringOutput());
	}

	private string toStringState(){
		string res = "{";
		for (int i = 0; i < nbFeatures - 1; i++) {
			res += string.Format("{0:N2}",currentState[i]) + ",";
		}
		return res + string.Format("{0:N2}",currentState[nbFeatures - 1]) + "}";
	}

	private string toStringOutput(){
		string res = "{";
		for (int i = 0; i < nbActions - 1; i++) {
			res += string.Format("{0:N2}",network.Output [i]) + "|";
		}
		return res + string.Format("{0:N2}",network.Output [nbActions - 1]) + "}";
	}
}

