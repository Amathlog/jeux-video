﻿using UnityEngine;
using System.Collections;

public class StateScriptBis : MonoBehaviour {

	public int StateNumber { get; set; }
	private bool triggered;
	private LearningAgentBis agent;

	void Start(){
		agent = GameObject.FindGameObjectWithTag ("Agent").GetComponent<LearningAgentBis> ();
		reset ();
	}

	public void reset(){
		triggered = false;
	}


	void OnTriggerEnter(Collider col){
		if (!enabled)
			return;
		if(col.transform.parent.parent.gameObject.CompareTag("Agent") & !triggered){
			if (transform.parent.childCount - 1 == StateNumber)
				Debug.Log ("Last State at : " + Time.time);
			agent.boxEncoutered (triggered, Vector3.Angle(transform.forward, col.transform.parent.parent.forward));
			triggered = true;
		}
	}
}
