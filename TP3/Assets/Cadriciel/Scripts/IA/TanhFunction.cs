﻿using System;
using AForge.Neuro;

[Serializable]
public class TanhFunction : IActivationFunction, ICloneable{

	public double Function(double x){
		double aux = Math.Exp (x);
		aux = aux * aux;

		return (aux - 1) / (aux + 1);
	}

	public double Derivative(double x){
		double y = Function (x);

		return 1 - y * y;
	}

	public double Derivative2(double y){
		return 1 - y * y;
	}

	public object Clone(){
		return new TanhFunction ();
	}

	public override string ToString ()
	{
		return string.Format ("[TanhFunction]");
	}

}
