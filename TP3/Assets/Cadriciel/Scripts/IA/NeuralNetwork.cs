﻿using System.Collections;

public class NeuralNetwork{
	
	private int inputs;
	private int hiddenLayers;
	private int outputs;
	private Matrix[] a;
	private Matrix[] z;
	private Matrix[] thetas;

	public float learningRate{ get; set;}
	public float lambda{ get; set;}

	private float sigmoidFunction(float value){
		return 1.0f / (1.0f + (float)System.Math.Exp (-value));
	}

	private Matrix sigmoidFunction(Matrix m){
		Matrix res = new Matrix (m.rows, m.cols);
		for (int i = 0; i < m.rows; i++) {
			for (int j = 0; j < m.cols; j++) {
				res [i, j] = sigmoidFunction (m [i, j]);
			}
		}
		return res;
	}

	private Matrix sigmoidGradient(Matrix m){
		Matrix aux = sigmoidFunction (m);
		return Matrix.inPlaceMult (aux, Matrix.filled (1.0f, m.rows, m.cols) - aux);
	}

	public Matrix forwardPropagation(Matrix input){
		a = new Matrix[hiddenLayers + 2];
		z = new Matrix[hiddenLayers + 1];
		a [0] = input.addOnes(false);
		for(int i = 0; i < hiddenLayers + 1; i++){
			z [i] = a [i] * thetas [i].transpose();
			a [i + 1] = sigmoidFunction (z [i]).addOnes (false);
		}
		return sigmoidFunction (z [hiddenLayers - 1]);
	}

	private void backPropagation(Matrix error){
		Matrix[] delta = new Matrix[hiddenLayers + 1];
		Matrix[] theta_grad = new Matrix[hiddenLayers + 1];
		delta [hiddenLayers] = error;
		theta_grad [hiddenLayers] = delta [hiddenLayers] * a[hiddenLayers];

		for (int i = hiddenLayers - 1; i >= 0; i--) {
			delta [i] = Matrix.inPlaceMult (thetas [i].removeFirstCols ().transpose () * delta [i + 1], sigmoidGradient (z [i].transpose ()));
			theta_grad [i] = delta [i] * a [i];
		}
		for (int i = 0; i < hiddenLayers + 1; i++) {
			thetas [i] = thetas [i] - learningRate * theta_grad [i];
		}
	}

}
