﻿using UnityEngine;
using System.Collections;

//15 actions possible : Nothing/Half-Accelerate/Accelerate and Right/Left (and their half action)
enum Actions{
	NOTHING = 0,
	H_ACCELERATE = 1,
	ACCELERATE = 2,
	H_RIGHT = 3,
	H_LEFT = 6,
	RIGHT = 9,
	LEFT = 12
}

public class LearningAgent : MonoBehaviour {


	public int nbActions = 15;					//Number of actions possibles
	public string fileName;						//Name of the file where the QMatrix will be written into
	public GameObject statesBoxes;				//Reference to the gameObject where all the boxes are into
	public float distanceFromCenter = 3.0f;		//Determine the size of the middle box

	public bool learning = true;				//Is the agent is learning ?

	private QLearning qLearning;				//Reference to the Q-Learning class

	private CarController car;					//Reference to the car agent

	private int lastState;						//Last state of our modelisation
	private int nbStates;						//Number of states of our modelisatioin
	private int currentState = 0;				//Current state of the simulation
	private int currentAction = 2;				//Current action

	private double timeOutAction = 2.0;			//Time before reloading the level (and blame the agent for that)
	private double previousStateTime = 0.0;		//Keep trace of the time
	private double currentTime = 0.0;			//Use to know the currentTime

	// Use this for initialization
	void Start () {
		qLearning = new QLearning ();
		//We have 3 boxes into 1 => 3times more states
		nbStates = statesBoxes.transform.childCount * 3;
		lastState = nbStates - 1;
		//Give to each box its number
		int childCount = 0;
		foreach (Transform child in statesBoxes.transform) {
			child.GetComponent<StateScript> ().StateNumber = childCount;
			childCount++;
		}
		//Get the reference to the car agent
		car = GetComponent<CarController>();
		//Initialisation of QLearning
		qLearning.init (fileName, nbStates, nbActions, lastState);
		//Keep trace of the time
		resetTime ();
	}

	//Called when the object is destroyed
	void OnDestroy(){
		if (!enabled)
			return;
		qLearning.Save (fileName);
	}

	//Called at everyframe
	//Here we keep an eye on time
	void Update(){
		currentTime = Time.time;
		if (currentTime > previousStateTime + timeOutAction) {
			updateState (currentState);
		}
	}

	//Called at every frame
	//Execute the currentAction
	void FixedUpdate(){
//		Debug.Log ("Action = " + currentAction);
		float h = 0;
		float v = 0;
		if (currentAction % 3 == 1)
			v = 0.5f;
		else if (currentAction % 3 == 2)
			v = 1;
		if (currentAction / 3 == 1)
			h = 0.5f;
		else if (currentAction / 3 == 2)
			h = -0.5f;
		else if (currentAction / 3 == 3)
			h = 1;
		else if (currentAction / 3 == 4)
			h = -1;
//		Debug.Log ("h = " + h);
//		Debug.Log ("v = " + v);
		car.Move(h,v,0,false,false);
		if (car.CurrentSpeed < 0)
			updateState (currentState);
	}

	//Reload the level
	public void reset(){
		Application.LoadLevel (Application.loadedLevel);
	}

	//Called everytime a state is updated
	public void updateState(int newState, double angle = 0.0, double distance = 0.0, bool isRight = false){
		if (learning) {
			//3 states in each box 0 = left, 1 = middle, 2 = right
			newState*=3;
			if (distance < distanceFromCenter)
				newState += 1;
			else if (isRight)
				newState += 2;
					
			//In case the agent didn't follow the order...
			if (newState/3 > currentState/3 + 1)
				newState = currentState;
					
			//		Debug.Log ("Previous State = " + currentState);
			//		Debug.Log ("New State = " + newState);
			//		Debug.Log ("Old Q Value = " + qLearning.Q(currentState, currentAction));
			qLearning.updateQ (currentState, currentAction, rewardFunction (newState, currentState == newState, angle, distance), newState, true);
			//		Debug.Log ("New Q Value = " + qLearning.Q(currentState, currentAction));
			if (newState == lastState || newState == currentState) {
				reset ();
			} else {
				currentState = newState;
				currentAction = qLearning.chooseNextAction (currentState, false);
				showStateAction ();
				resetTime ();
			}
			//		Debug.Log (currentAction);
		} else {
			currentState = newState;
			currentAction = qLearning.chooseNextAction (currentState);
			showStateAction ();
		}

	}

	private double rewardFunction(int state, bool sameState, double angle, double distance){
		if (sameState || angle > 90.0)
			return -5.0;
		if (state == lastState)
			return 10.0;
		//TODO : Affecting different rewards if into left or right box about the angle
		return 0.5 * ((distanceFromCenter - distance) + 3*(1.0 - angle/30.0));
	}

	private void resetTime(){
		previousStateTime = Time.time;
		currentTime = previousStateTime;
	}

	private void showStateAction(){
		string res = "";
		if (currentAction % 3 == 1)
			res += "HALF ACCELERATE ";
		else if (currentAction % 3 == 2)
			res += "ACCELERATE ";
		else
			res += "NO ACCELERATION ";
		if (currentAction / 3 == 1)
			res += "HALF RIGHT";
		else if (currentAction / 3 == 2)
			res += "HALF LEFT";
		else if (currentAction / 3 == 3)
			res += "RIGHT";
		else if (currentAction / 3 == 4)
			res += "LEFT";
		else
			res += "FORWARD";
		string box = "";
		int aux = currentState % 3;
		switch (aux) {
			case 0:
				box = "LEFT";
				break;
			case 1:
				box = "MIDDLE";
				break;
		default:
			box = "RIGHT";
			break;
		}
		Debug.Log ("Current state : " + currentState/3 + " Box : " + box + " Action : " + res + " Time Elapsed : " + (currentTime - previousStateTime) + "s");
	}
}
