﻿using UnityEngine;
using System.Collections;

public class StateScript : MonoBehaviour {

	public int StateNumber { get; set; }		//Property for the number of the box
	private bool triggered;						//Already triggered ?
	private LearningAgent agent;				//Reference to the agent
	private Vector3 contactPoint;				//Useful for gizmos
	private Vector3 forwardCar;					//Useful for gizmos

	//Called at the init
	void Start(){
		agent = GameObject.FindGameObjectWithTag ("Agent").GetComponent<LearningAgent> ();
		reset ();
	}

	//Reset the triggered attribute
	public void reset(){
		triggered = false;
	}

	//Called on collision
	void OnTriggerEnter(Collider col){
		//Check if the collisioner is the agent
		if(col.transform.parent.parent.gameObject.CompareTag("Agent") && !triggered){
			triggered = true;

			//Compute the angle the car enter the box
			forwardCar = col.collider.transform.parent.parent.transform.forward;
			double angle = Vector3.Angle (forwardCar, transform.forward);
			//Debug.Log("Angle : " + angle);

			//Compute the distance between the collision point (between the car and the box) and the center of the box
			contactPoint = col.ClosestPointOnBounds(transform.position);
			float distance = Vector3.Distance (contactPoint, transform.position);
			//Debug.Log("Distance : " + distance);

			//Compute if the car is at the right side or the left side of the box
			bool isRight = Vector3.Dot(Vector3.Cross(transform.position - contactPoint, transform.forward), transform.up) >= 0.0f;

			//Update the state with all those informations
			if (!enabled)
				return;
			agent.updateState (StateNumber, angle, distance, isRight);
		}
	}

	//Colorful informations (See it in the Scene Window while in game)
	void OnDrawGizmos(){
		Gizmos.color = new Color (0, 1, 1, 0.3f);
		if(agent != null)
			Gizmos.DrawSphere (transform.position, agent.distanceFromCenter);
		if (contactPoint != Vector3.zero) {
			float distance = Vector3.Distance (contactPoint, transform.position);
			if (distance <= 3.0f)
				Gizmos.color = Color.red;
			else{
				if (Vector3.Dot(Vector3.Cross(transform.position - contactPoint, transform.forward), transform.up) >= 0.0f)
					//RIGHT
					Gizmos.color = Color.blue;
				else
					//LEFT
					Gizmos.color = Color.green;
				}
			Gizmos.DrawSphere (contactPoint, 1);
		}
		if(forwardCar != Vector3.zero){
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (transform.position, transform.position + 5*transform.forward);
			Gizmos.color = Color.magenta;
			Gizmos.DrawLine (transform.position, transform.position + 5*forwardCar);
		}
	}
}
